#!/usr/bin/env python3
from os import rename
from pathlib import Path

PATH_TO_MINECRAFT_FOLDER = Path("..")

class bcolors:
    OKBLUE = '\033[34m'
    ERROR = '\033[31m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'

def fileSwitcher( wantedVersionFile, unwantedVersionFile):
    rename(PATH_TO_MINECRAFT_FOLDER / "options.txt", unwantedVersionFile )
    rename( wantedVersionFile, PATH_TO_MINECRAFT_FOLDER / "options.txt" )

oldVersionOptionsFile = PATH_TO_MINECRAFT_FOLDER / "optionsBelowVersion1.13.txt"
newestVersionOptionsFile = PATH_TO_MINECRAFT_FOLDER / "optionsAboveVersion1.13.txt"

if oldVersionOptionsFile.exists():
    print(f"you will now be able to play minecraft{bcolors.OKBLUE}{bcolors.BOLD} BELOW Version 1.13{bcolors.ENDC}")
    fileSwitcher( wantedVersionFile = oldVersionOptionsFile, unwantedVersionFile = newestVersionOptionsFile)
elif newestVersionOptionsFile.exists():
    print(f"you will now be able to play minecraft{bcolors.OKBLUE}{bcolors.BOLD} Version 1.13 and ABOVE{bcolors.ENDC}")
    fileSwitcher( wantedVersionFile = newestVersionOptionsFile, unwantedVersionFile = oldVersionOptionsFile)
else:
    print(f"{bcolors.ERROR}{bcolors.BOLD} ERROR, couldn't find a file to switch with.{bcolors.ENDC}")
    print(f"{bcolors.ERROR}If you last played a minecraft version >= 1.13, make sure you have a file called optionsBelowVersion1.13.txt in your minecraft folder that contains your settings for Minecraft versions below 1.13{bcolors.ENDC}")
    print(f"{bcolors.ERROR}If you last played a minecraft version below 1.13, make sure you have a file called optionsAboveVersion1.13.txt in your minecraft folder that contains your settings for Minecraft versions >= 1.13{bcolors.ENDC}")