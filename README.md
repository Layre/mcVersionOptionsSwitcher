# mcVersionOptionsSwitcher
## Description
In the Version 1.13, Minecraft changed the Layout of the options.txt file, which contains the controls and other options. Therefore your controls get set back to default, when you switch to a MC version below/above 1.13.

mcVersionOptionsSwitcher makes it easier to switch between those versions, by letting you store your settings for the unused Version and exchanging them to your needs.

## Installation
clone the  mcVersionOptionsSwitcher in your Minecraft folder.

If you last played a minecraft version >= 1.13, make sure you have a file called optionsBelowVersion1.13.txt in your minecraft folder that contains your settings for Minecraft versions below 1.13.

If you last played a minecraft version below 1.13, make sure you have a file called optionsAboveVersion1.13.txt in your minecraft folder that contains your settings for Minecraft versions >= 1.13.

## Running
To run the script, simply type

```python3 mcVersionOptionsSwitcher.py```

in your Terminal.
Make sure you are in the mcVersionoptionsSwitcher folder with your Terminal.
Note that this script needs Python 3.4+ to function out of the box.

Alternatively, **I recommend building a Bash file**, so you can change the Versions with one click.

## Usage
Run this script before you plan to launch a different minecraft version than before. It will tell you wether you are ready to play Minecraft below 1.13 or above.